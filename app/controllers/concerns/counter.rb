module Counter
  extend ActiveSupport::Concern

  private

  def get_counter
    session[:counter] || 0
  end

  def set_counter(value)
    @counter = value
    session[:counter] = @counter
  end

  def inc_counter
    @counter = get_counter + 1
    set_counter @counter
  end
end

