class StoreController < ApplicationController
  include CurrentCart, Counter
  before_action :inc_counter, only: [:index]
  before_action :set_cart

  def index
    @products = Product.order(:title)
  end
end
